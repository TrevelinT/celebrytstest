const resolve = require('path').resolve;
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

const commonConfig = require('./base.js');

module.exports = function() {
    return webpackMerge(commonConfig(), {
        module: {
            loaders: [
                {
                    test: /\.scss$/,
                    use: ['style-loader', 'css-loader', 'sass-loader'],
                    exclude: /node_modules/
                }
            ]
        },
        devtool: 'inline-source-map',
        devServer: {
            historyApiFallback: true,
            stats: 'errors-only',
            port: '8000',
            host: 'localhost',
            hot: true,
            contentBase: resolve(__dirname, '../dist'),
            publicPath: '/'
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NamedModulesPlugin()
        ]
    });
};
