import React from 'react';

/* eslint-disable no-unused-vars */
import css from '../../styles/main.scss';
import Pagination from '../Pagination/pagination.container';

const App = () => (
    <div className="app">
        <h1 className="title">Google pagination</h1>
        <Pagination />
    </div>
);

export default App;
