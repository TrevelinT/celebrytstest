import { connect } from 'react-redux';
import { incrementCounter, decrementCounter, changeCounter } from './pagination.actions';
import PaginationList from './paginationList';

const mapStateToProps = ({ currentPage, maxPagination }) => {
    return {
        currentPage,
        maxPagination
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPageClicked: (page) => {
            dispatch(changeCounter(page));
        },
        onNextPageClicked: () => {
            dispatch(incrementCounter());
        },
        onPrevPageClicked: () => {
            dispatch(decrementCounter());
        }
    };
};

const Pagination = connect(
    mapStateToProps,
    mapDispatchToProps
)(PaginationList);

export default Pagination;
