import React, { PropTypes } from 'react';

const Pagination = ({ onClick, isActive, page }) => (
    <li className={'pagination-item ' + (isActive ? 'pagination-current' : '')}>
        <a className="pagination-link" href="#" onClick={onClick}>
            <span className="pagination-icon"></span>
            {page}
        </a>
    </li>
);

Pagination.propTypes = {
    onClick: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired,
    page: PropTypes.number.isRequired
};

export default Pagination;