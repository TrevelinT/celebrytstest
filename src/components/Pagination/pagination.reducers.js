import {
    INCREMENT_COUNTER,
    DECREMENT_COUNTER,
    CHANGE_COUNTER
} from './pagination.actions';

const initialState = {
    currentPage: 0,
    maxPagination: 9
};

export default function pagination(state = initialState, action) {
    switch(action.type) {
    case INCREMENT_COUNTER:
        return Object.assign({}, state, {
            currentPage: state.currentPage + 1 > state.maxPagination ? state.currentPage : state.currentPage + 1
        });
    case DECREMENT_COUNTER:
        return Object.assign({}, state, {
            currentPage: state.currentPage - 1 < 0 ? state.currentPage : state.currentPage - 1
        });
    case CHANGE_COUNTER:
        return Object.assign({}, state, {
            currentPage: (action.payload < 0 || action.payload > state.maxPagination) ? state.currentPage : action.payload
        });
    default:
        return state;
    }
}
