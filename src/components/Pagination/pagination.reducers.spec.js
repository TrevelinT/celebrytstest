import 'babel-polyfill';
import { expect } from 'chai';
import pagination from './pagination.reducers';

describe('reducers', () => {
    describe('pagination', () => {
        const initialState = {
            currentPage: 0,
            maxPagination: 9
        };

        it('should provide the initial state', () => {
            expect(pagination(undefined, {})).to.deep.equal(initialState);
        });

        it('should handle INCREMENT_COUNTER action', () => {
            expect(pagination(initialState, { type: 'INCREMENT_COUNTER' })).to.deep.equal({
                currentPage: 1,
                maxPagination: 9
            });
        });

        it('should handle CHANGE_COUNTER action', () => {
            expect(pagination(initialState, { type: 'CHANGE_COUNTER', payload: 6 })).to.deep.equal({
                currentPage: 6,
                maxPagination: 9
            });
        });

        it('should handle DECREMENT_COUNTER action', () => {
            let state = Object.assign({}, initialState, {
                currentPage: 6
            });
            expect(pagination(state, { type: 'DECREMENT_COUNTER' })).to.deep.equal({
                currentPage: 5,
                maxPagination: 9
            });
        });

        it('should return the same page when CHANGE_COUNTER action is used with numbers beside the pagination range', () => {
            expect(pagination(initialState, { type: 'CHANGE_COUNTER', payload: 12 })).to.deep.equal({
                currentPage: 0,
                maxPagination: 9
            });
        });

        it('should return the first page if INCREMENT_COUNTER action is used when the last page been selected', () => {
            let state = Object.assign({}, initialState, {
                currentPage: 9
            });
            expect(pagination(state, { type: 'INCREMENT_COUNTER' })).to.deep.equal({
                currentPage: 9,
                maxPagination: 9
            });
        });

        it('should return the first page if DECREMENT_COUNTER action is used when the first page been selected', () => {
            expect(pagination(initialState, { type: 'DECREMENT_COUNTER' })).to.deep.equal({
                currentPage: 0,
                maxPagination: 9
            });
        });

    });
});
