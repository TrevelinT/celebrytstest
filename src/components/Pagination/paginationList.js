import React, { PropTypes } from 'react';
import Pagination from './pagination';

const PaginationList = ({ currentPage, maxPagination, onPageClicked, onNextPageClicked, onPrevPageClicked }) => {
    const pages = Array.from(
        { length: maxPagination + 1 },
        (v, i) => i + 1
    );

    return (
        <ul className="pagination">
            {currentPage !== 0
            ? (
                <li className="pagination-item pagination-start pagination-active">
                    <a 
                        href="#" 
                        className="pagination-link"
                        onClick={e => {
                            e.stopPropagation();
                            onPrevPageClicked();
                        }}
                    >
                    <span className="pagination-icon"></span>
                    <span className="pagination-text">Anterior</span>
                    </a>
                </li>
                )
                : (
                <li className="pagination-item pagination-start">
                    <span className="pagination-icon"></span>
                </li>
                )
            }
            {pages.map((page, index) =>
                <Pagination
                    key={index}
                    page={page}
                    isActive={ index === currentPage }
                    onClick={e => {
                        e.stopPropagation();
                        onPageClicked(index);
                    }} 
                />
            )}
            {currentPage !== maxPagination
            ? (
                <li className="pagination-item pagination-end pagination-active">
                    <a
                        href="#"
                        className="pagination-link"
                        onClick={e => {
                            e.stopPropagation();
                            onNextPageClicked();
                        }}
                    >
                    <span className="pagination-icon"></span>
                    <span className="pagination-text">Mais</span>
                    </a>
                </li>
                )
                : (
                <li className="pagination-item pagination-end">
                    <span className="pagination-icon"></span>
                </li>
                )
            }
        </ul>
    );
};

PaginationList.propTypes = {
    onPageClicked: PropTypes.func.isRequired,
    onNextPageClicked: PropTypes.func.isRequired,
    onPrevPageClicked: PropTypes.func.isRequired,
    currentPage: PropTypes.number.isRequired,
    maxPagination: PropTypes.number.isRequired
};

export default PaginationList;